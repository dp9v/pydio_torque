<?php
/*
 * Copyright 2007-2013 Charles du Jeu - Abstrium SAS <team (at) pyd.io>
 * This file is part of Pydio.
 *
 * Pydio is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Pydio is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with Pydio.  If not, see <http://www.gnu.org/licenses/>.
 *
 * The latest code can be found at <http://pyd.io/>.
 *
 */
defined('AJXP_EXEC') or die( 'Access not allowed');


// This is used to catch exception while downloading
if (!function_exists('download_exception_handler')) {
  function download_exception_handler($exception){}
}
/**
 * AJXP_Plugin to access a filesystem. Most "FS" like driver (even remote ones)
 * extend this one.
 * @package AjaXplorer_Plugins
 * @subpackage Access
 */
class fsAccessDriver_remote extends AbstractAccessDriver implements AjxpWrapperProvider
{
  /**
   * @var Repository
   */
  public $repository;
  public $driverConf;
  protected $wrapperClassName;
  protected $urlBase;

  public function nodeChanged(&$from, &$to, $copy = false){
    fsAccessWrapper_remote::sendMessage("Функция не реализоавана (nodeChanged)");
  }

  public function nodeWillChange($node, $newSize = null){
    fsAccessWrapper_remote::sendMessage("Функция не реализоавана (nodeWillChange)");
  }

  public function initRepository()
  {

  }

  public function getResourceUrl($path)
  {
    return $this->urlBase.$path;
  }

  public function getWrapperClassName()
  {
    return $this->wrapperClassName;
  }

  protected function getNodesDiffArray()
  {
    return array("REMOVE" => array(), "ADD" => array(), "UPDATE" => array());
  }

  public function switchAction($action, $httpVars, $fileVars)
  {
    $domain = $_SESSION['domain_path'] OR "";
    if($domain==="")
      fsAccessWrapper_remote::sendMessage("Ошибка: Требуется повторная авторизация на сервере");
    if(!isSet($this->actions[$action])) return;
    
    parent::accessPreprocess($action, $httpVars, $fileVars);
    $selection = new UserSelection($this->repository);
    $dir = $httpVars["dir"] OR "";
    if ($this->wrapperClassName == "fsAccessWrapper") {
      $dir = fsAccessWrapper_remote::patchPathForBaseDir($dir);
    }
    $dir = AJXP_Utils::securePath($dir);
    if ($action != "upload") {
      $dir = AJXP_Utils::decodeSecureMagic($dir);
    }
    $selection->initFromHttpVars($httpVars);
    $mess = ConfService::getMessages();

    if(isSet($newArgs["action"])) $action = $newArgs["action"];
    if(isSet($newArgs["dest"])) $httpVars["dest"] = SystemTextEncoding::toUTF8($newArgs["dest"]);//Re-encode!
    // FILTER DIR PAGINATION ANCHOR
    $page = null;
    if (isSet($dir) && strstr($dir, "%23")!==false) {
      $parts = explode("%23", $dir);
      $dir = $parts[0];
      $page = $parts[1];
    }

    $pendingSelection = "";
    $logMessage = null;
    $reloadContextNode = false;

    switch ($action) {

      //------------------------------------
      //  ONLINE EDIT
      //------------------------------------

      //  Получение содержимого файла
      case "get_content":
        $fileName = $selection->getUniqueFile();
        $response = fsAccessWrapper_remote::getRemoteText($fileName);
        echo $response;
        break;

      //  Запись текста в файл
      case "put_content":
        if(!isset($httpVars["content"])) break;
        // Load "code" variable directly from POST array, do not "securePath" or "sanitize"...
        $code = $httpVars["content"];
        $file = $selection->getUniqueFile();
        $fileParts = $this->splitName($file);

        $tmpName = uniqid();
        $f = fopen($tmpName,'w');
        fwrite($f, $code);
        fclose($f);
        $json_response = fsAccessWrapper_remote::uploadFileCommand(realpath($tmpName), $fileParts['name'],$fileParts['dir']);
        unlink($tmpName);

        if ($json_response->success!=='true')
          fsAccessWrapper_remote::sendMessage("Ошибка сохранения файла: $file");
        else
          fsAccessWrapper_remote::sendMessage("Изменения успешно сохранены: $file", "SUCCESS");

        return;



      //  Перемещение файлов и директорий
      case "move";
        $files = $selection->getFiles();
        $dest = AJXP_Utils::decodeSecureMagic($httpVars["dest"]);
        $logMessages = array();

        if(!isSet($nodesDiffs)) $nodesDiffs = $this->getNodesDiffArray();
        foreach($files as $file)
        {
          $file_parts = $this->splitName($file);
          $filename_new = $dest.'/'.$file_parts["name"];

          $message = $this->move($dest, $file);
          if (is_null($message))
          {
            array_push($logMessages, "Файл $file успешно перемещен в $dest");
            $newNode = new AJXP_Node($filename_new);
            array_push($nodesDiffs["ADD"], $newNode);
            array_push($nodesDiffs["REMOVE"], $file);
          }
          else
            array_push($logMessages, $message);
        }
        $logMessage = implode("<br>", $logMessages);
        break;

      //  Удаление файлов и директорий
      case "delete";

        $files = $selection->getFiles();
        $logMessages = array();
        if(!isSet($nodesDiffs)) $nodesDiffs = $this->getNodesDiffArray();
        foreach($files as $file)
        {
          $message = $this->delete($file);
          if(is_null($message))
          {
            array_push($nodesDiffs["REMOVE"], $file);
            $message = "Файл $file успешно удален";
          }
          array_push($logMessages, $message);
        }
        $logMessage = implode("<br>", $logMessages);
        break;

      //  переимкнование
      case "rename";
        $file = $selection->getUniqueFile();
        $filename_new = AJXP_Utils::decodeSecureMagic($httpVars["filename_new"]);
        $file_parts = $this->splitName($file);
        $dest = $file_parts['dir']."/".$filename_new;
        if(!isSet($nodesDiffs)) $nodesDiffs = $this->getNodesDiffArray();

        $message = $this->rename($file, $dest);
        if (is_null($message))
        {
          $logMessage = "Файл $file успешно переименован в $filename_new";
          $newNode = new AJXP_Node($dest);
          $nodesDiffs["UPDATE"][$file] = $newNode;
        }
        else
          fsAccessWrapper_remote::sendMessage($message);
        break;

      //  Создание директории
      case "mkdir";

        $files = $selection->getFiles();
        if(isSet($httpVars["dirname"])){
          $files[] = $dir ."/". AJXP_Utils::decodeSecureMagic($httpVars["dirname"], AJXP_SANITIZE_FILENAME);
        }
        $messages = array();
        $errors = array();
        foreach($files as $newDirPath){
          $file = $this->splitName($newDirPath);
          $error = $this->mkDir($file['dir'], $file['name']);
          if (isSet($error)) {
            $errors[] = $error;
            continue;
          }
          $messtmp="Папка ".$file['name']." успешно создана в ".$file['dir'];
          $messages[] = $messtmp;
          $newNode = new AJXP_Node($newDirPath);
          $this->loadNodeInfo($newNode, $newDirPath);
          if(!isSet($nodesDiffs)) $nodesDiffs = $this->getNodesDiffArray();
          array_push($nodesDiffs["ADD"], $newNode);
        }
        if(count($errors)){
          if(!count($messages)){
            throw new AJXP_Exception(implode('', $errors));
          }else{
            $errorMessage = implode("<br>", $errors);
          }
        }
        $logMessage = implode("<br>", $messages);
        break;

      //  Создание пустого файла
      case "mkfile";

        if(empty($httpVars["filename"]) && isSet($httpVars["node"])){
          $filename=AJXP_Utils::decodeSecureMagic($httpVars["node"]);
        }else{
          $filename=AJXP_Utils::decodeSecureMagic($httpVars["filename"], AJXP_SANITIZE_FILENAME);
        }

        $message = $this->createEmptyFile($dir,$filename);
        if(is_null($message))
        {
          $newNode = new AJXP_Node($dir."/".$filename);
          $this->loadNodeInfo($newNode,$dir."/".$filename,0,false);
          if(!isSet($nodesDiffs)) $nodesDiffs = $this->getNodesDiffArray();
          array_push($nodesDiffs["ADD"], $newNode);
        }
        else
          fsAccessWrapper_remote::sendMessage($message);

        break;

      //  Загрузка файла на ВК
      case "upload":
        $destination=$this->urlBase.AJXP_Utils::decodeSecureMagic($dir);
        foreach ($fileVars as $boxName => $boxData)
        {
          $userfileName = $boxData["name"];
          $tmpName = $boxData["tmp_name"];
          $fileSize = $boxData["size"];
          $json_response =fsAccessWrapper_remote::uploadFileCommand($tmpName, $userfileName, $destination);
          if ($json_response->success==='true')
          {
            $newNode = new AJXP_Node($destination.'/'.$userfileName);
            $this->loadNodeInfo($newNode,$userfileName,$fileSize,false,'0777');
            return array("SUCCESS" => true, "CREATED_NODE" => $newNode);
          }
          else
            return array("ERROR" => array("CODE" => 0, "MESSAGE" => "Ошибка загрузки файла $userfileName"));
        }
        break;

      //  Получение списка фалов и директорий
      case "ls":
        $url = '/v1/fs/list';
        $file = $httpVars["file"] OR "";
        $json_response = fsAccessWrapper_remote::remoteCommand($url, 'POST', array("path" => $dir));
        if ($json_response->success==='true')
        {
          $parentAjxpNode = new AJXP_Node($dir, array());
          $parentAjxpNode->loadNodeInfo($parentAjxpNode, $dir);
          AJXP_XMLWriter::renderAjxpHeaderNode($parentAjxpNode);
          foreach ($json_response->list->dirs as $meta)
          {
            if($file=="" or $file==$meta->name)
            {
              $fileName = str_replace('//', '/', $dir . '/' . $meta->name);
              $node = new AJXP_Node($fileName, array());
              $this->loadNodeInfo($node, $fileName, 0, true, $meta->mode);
              AJXP_XMLWriter::renderAjxpNode($node);
            }
          }
          foreach ($json_response->list->files as $meta)
          {
            if($file=="" or $file==$meta->name)
            {
              $fileName = str_replace('//', '/', $dir . '/' . $meta->name);
              $node = new AJXP_Node($fileName, array());
              $this->loadNodeInfo($node, $fileName, $meta->size, false, $meta->mode);
              AJXP_XMLWriter::renderAjxpNode($node);
            }
          }
          AJXP_XMLWriter::close();
        }
        else
        {
          fsAccessWrapper_remote::sendMessage("Ошибка просмотра директории");
        }
        break;
    }


    $xmlBuffer = "";
    if (isset($logMessage) || isset($errorMessage)) {
      $xmlBuffer .= AJXP_XMLWriter::sendMessage((isSet($logMessage)?$logMessage:null), (isSet($errorMessage)?$errorMessage:null), false);
    }
    if ($reloadContextNode) {
      if(!isSet($pendingSelection)) $pendingSelection = "";
      $xmlBuffer .= AJXP_XMLWriter::reloadDataNode("", $pendingSelection, false);
    }
    if (isSet($reloadDataNode)) {
      $xmlBuffer .= AJXP_XMLWriter::reloadDataNode($reloadDataNode, "", false);
    }
    if (isSet($nodesDiffs)) {
      $xmlBuffer .= AJXP_XMLWriter::writeNodesDiff($nodesDiffs, false);
    }

    return $xmlBuffer;
  }

  public function splitName($fullName)
  {
    if($fullName==='/')
      return array('dir'=>'', 'name'=>'/');
    $parts = explode('/', $fullName);
    $fileName = array_pop($parts);
    $dir = "";
    foreach ($parts as $part)
      $dir .= '/'.$part;
    $dir = str_replace('//','/',$dir);
    return array('dir'=>$dir, 'name'=>$fileName);
  }

  public function loadNodeInfo(&$ajxpNode, $fileName, $byteSize=0, $isDir=true, $fPerms='0777')
  {
    $isLeaf = !$isDir;
    $metaData = array();
    $metaData["is_file"] = $isDir ? false : true;
    $metaData["filename"] = $fileName;
    $metaData["text"] = $this->splitName($fileName);
    $metaData["text"] = $metaData["text"]['name'];

    $mimeData = AJXP_Utils::mimeData($fileName, !$isLeaf);
    $metaData["mimestring_id"] = $mimeData[0];
    $metaData["icon"] = $mimeData[1];
    if ($metaData["icon"] == "folder.png") {
      $metaData["openicon"] = "folder_open.png";
      $metaData["ajxp_mime"] = "ajxp_folder";
    }
    if (!$this->isText($mimeData[0]))
      $metaData["ajxp_mime"] = "ajxp_folder";
    $metaData["file_group"] = "unknown";
    $metaData["file_owner"] = "unknown";
    //$metaData["ajxp_readonly"] = !($this->isWriteable($fileName));
    $metaData["file_perms"] = $fPerms;
    $metaData["ajxp_modiftime"] = $this->date_modif($fileName);
    $metaData["ajxp_description"] =$metaData["ajxp_relativetime"] = "";
    $metaData["bytesize"] = $byteSize;
    $metaData["filesize"] = AJXP_Utils::roundSize($metaData["bytesize"]);
    $ajxpNode->mergeMetadata($metaData);
  }

  public function date_modif($file)
  {
    $tmp = 0;
    return $tmp;// date("d,m L Y H:i:s",$tmp);
  }

  public function move($destDir, $filePath)
  {
    $url = "/v1/fs/rename";
    $file_parts = $this->splitName("$filePath");
    $json_response = fsAccessWrapper_remote::remoteCommand($url, "POST", array("oldpath" => $filePath,
                                  "newpath" => $destDir.'/'.$file_parts["name"]));
    if($json_response->success !== 'true')
    {
      return "Ошибка переименования файла";
    }
    return null;
  }

  public function rename($filePath, $newFileName)
  {
    $url = "/v1/fs/rename";
    $file_parts = $this->splitName("$filePath");
    $json_response = fsAccessWrapper_remote::remoteCommand($url, "POST", array("oldpath" => $filePath,
                                  "newpath" => $newFileName));
    if($json_response->success !== 'true')
    {
      return "Ошибка переименования файла";
    }
    return null;
  }

  public function mkDir($destDir, $newDirName)
  {
    $url = "/v1/fs/mkdir";
    $json_response = fsAccessWrapper_remote::remoteCommand($url, 'POST', array("path" => $destDir . '/' . $newDirName));
    if($json_response->success==='true')
      return null;
    else
      return "Ошибка созднаия директории";
  }

  public function createEmptyFile($destDir, $newFileName)
  {
    $tmpName = uniqid();
    $f = fopen($tmpName, 'w');
    fwrite($f," ");
    fclose($f);
     $json_response = fsAccessWrapper_remote::uploadFileCommand(stream_resolve_include_path($tmpName),$newFileName,$destDir);
    unlink($tmpName);
    if ($json_response->success!=='true')
      return "Ошибка создания файла: $newFileName";
    return null;

  }

  public function delete($filePath)
  {
    $url = '/v1/fs/delete';
    $json_response = fsAccessWrapper_remote::remoteCommand($url, "POST", array("path" => $filePath));
    if ($json_response->success!=='true')
      return "Ошибка удаления файла $filePath";
    return null;
  }

  public function isWriteable($dir)
  {
    return true;
  }

  public function isText($type)
  {
    $types = array(23, 10, 11, 14,15,21,22,58,64);
    return in_array($type, $types);
  }




}

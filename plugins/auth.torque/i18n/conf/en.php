<?php
/*
* Copyright 2007-2013 Charles du Jeu - Abstrium SAS <team (at) pyd.io>
* This file is part of Pydio.
*
* Pydio is free software: you can redistribute it and/or modify
* it under the terms of the GNU Affero General Public License as published by
* the Free Software Foundation, either version 3 of the License, or
* (at your option) any later version.
*
* Pydio is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
* GNU Affero General Public License for more details.
*
* You should have received a copy of the GNU Affero General Public License
* along with Pydio.  If not, see <http://www.gnu.org/licenses/>.
*
* The latest code can be found at <http://pyd.io/>.
*/
$mess=array(
"torque Directory" => "torque Directory",
"Authentication datas are stored on the torque server." => "Authentication datas are stored on the torque server.",
"torque URL" => "torque URL",
"torque Server URL (IP or name)" => "torque Server URL (IP or name)",
"torque Port" => "torque Port",
"torque Server Port (leave blank for default)" => "torque Server Port (leave blank for default)",
"torque bind username" => "torque bind username",
"Username (uid + dn) of torque bind user" => "Username (uid + dn) of torque bind user",
"torque bind password" => "torque bind password",
"Password of torque bind user" => "Password of torque bind user",
"People DN" => "People DN",
"DN where the users are stored" => "DN where the users are stored",
"torque Filter" => "torque Filter",
"Filter which users to fetch." => "Filter which users to fetch.",
"User attribute" => "User attribute",
"Username attribute" => "Username attribute",
"torque/AD Directory" => "torque/AD Directory",
"Authentication datas are stored in an torque/AD directory." => "Authentication datas are stored in an torque/AD directory.",
"Protocol" => "Protocol",
"Connect through torque or torques" => "Connect through torque or torques",
"Groups DN" => "Groups DN",
"DN where the groups are stored. Must be used in cunjonction with a group parameter mapping, generally using the memberOf feature." => "DN where the groups are stored. Must be used in cunjonction with a group parameter mapping, generally using the memberOf feature.",
"torque Groups Filter" => "torque Groups Filter",
"Filter which groups to fetch." => "Filter which groups to fetch.",
"Group attribute" => "Group attribute",
"Group main attribute to be used as a label" => "Group main attribute to be used as a label",
"torque attribute" => "torque attribute",
"Name of the torque attribute to read" => "Name of the torque attribute to read",
"Mapping Type" => "Mapping Type",
"Determine the type of mapping" => "Determine the type of mapping",
"Plugin parameter" => "Plugin parameter",
"Name of the custom local parameter to set" => "Name of the custom local parameter to set",
"Test User" => "Test User",
"Use the Test Connexion button to check if this user is correctly found in your torque directory." => "Use the Test Connection button to check if this user is correctly found in your torque directory.",
"Test Connexion" => "Test Connection",
"Try to connect to torque" => "Try to connect to torque",
);

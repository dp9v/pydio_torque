<?php
/*
 * Copyright 2007-2013 Charles du Jeu - Abstrium SAS <team (at) pyd.io>
 * This file is part of Pydio.
 *
 * Pydio is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Pydio is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with Pydio.  If not, see <http://www.gnu.org/licenses/>.
 *
 * The latest code can be found at <http://pyd.io/>.
 */
defined('AJXP_EXEC') or die('Access not allowed');

/**
 * Store authentication data in an SQL database
 * @package AjaXplorer_Plugins
 * @subpackage Auth
 */
class torqueAuthDriver extends AbstractAuthDriver {
  public $torqueDriver;
  public $driverName = "torque";

  public function init($options) {
    parent::init($options);
    /* $this->Driver = AJXP_Utils::cleanDibiDriverParameters($options["SQL_DRIVER"]);
     try {
         dibi::connect($this->sqlDriver);
     } catch (DibiException $e) {
         echo get_class($e), ': ', $e->getMessage(), "\n";
         exit(1);
     }
*/
  }

  public function getOption($optionName) {
    return (isSet($this->options[$optionName]) ?
      $this->options[$optionName] : "");
  }

  public function performChecks() {
  }

  public function supportsUsersPagination() {
    return false;
  }

  // $baseGroup = "/"
  public function listUsersPaginated($baseGroup, $regexp, $offset,
                                     $limit, $recursive = true) {
    return array();
  }

  public function findUserPage($baseGroup, $userLogin, $usersPerPage,
                               $offset) {
    return 0;
  }

  public function getUsersCount($baseGroup = "/", $regexp = "",
                                $filterProperty = null,
                                $filterValue = null,
                                $recursive = true) {
    return 1;
  }

  public function listUsers($baseGroup = "/") {
    return array();
  }

  public function userExists($login) {
    return true;
  }

  public function checkPassword($login, $pass, $seed) {
    //Получение пути расположения RESTfull web-сервиса
    $domain = $this->getOption("TORQUE_URL");
    $_SESSION['domain_path'] = $domain;
    $url = $domain . "/v1/auth/";

    //Формирование текста запроса к web-сервису
    $arr = array("login" => $login, "password" => $pass);
    $json_value = json_encode($arr);

    //Выполнение запроса к web-сервису
    $ch = curl_init($url);
    curl_setopt($ch, CURLOPT_HTTPHEADER,
      array("Content-Type: application/json"));
    curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
    curl_setopt($ch, CURLOPT_POST, true);
    curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, false);
    curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
    curl_setopt($ch, CURLOPT_POSTFIELDS, $json_value);
    $result = curl_exec($ch);
    curl_close($ch);

    //Обработка результатов запроса к web-сервису
    $json_response = json_decode($result);
    if ($json_response->success === "true") {
      //Сохранение токена в переменную среды
      $_SESSION['token'] = $json_response->token;
      return true;
    }
    else {
      return false;
    }
  }

  public function usersEditable() {
    return false;
  }

  public function passwordsEditable() {
    return false;
  }

  public function createUser($login, $passwd) {
    return false;
  }

  public function changePassword($login, $newPass) {
    return false;
  }

  public function deleteUser($login) {
    return false;
  }


}

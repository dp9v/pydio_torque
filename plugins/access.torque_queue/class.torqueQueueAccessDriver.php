<?php
/*
 * Copyright 2007-2013 Charles du Jeu - Abstrium SAS <team (at) pyd.io>
 * This file is part of Pydio.
 *
 * Pydio is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Pydio is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with Pydio.  If not, see <http://www.gnu.org/licenses/>.
 *
 * The latest code can be found at <http://pyd.io/>.
 *
 */
defined('AJXP_EXEC') or die('Access not allowed');


// This is used to catch exception while downloading
if (!function_exists('download_exception_handler')) {
  function download_exception_handler($exception) { }
}

/**
 * AJXP_Plugin to access a filesystem. Most "FS" like driver (even remote ones)
 * extend this one.
 * @package AjaXplorer_Plugins
 * @subpackage Access
 */
class torqueQueueAccessDriver extends AbstractAccessDriver
  implements AjxpWrapperProvider {
  /**
   * @var Repository
   */
  public $repository;
  public $driverConf;
  protected $wrapperClassName;
  protected $urlBase;

  public function nodeChanged(&$from, &$to, $copy = false) {
    $this->sendMessage("Функция не реализоавана (nodeChanged)");
  }

  public function createEmptyFile($path, $newDirName) {
    $this->sendMessage("Функция не реализоавана (createEmptyFile)");
  }

  public function nodeWillChange($node, $newSize = null) {
    $this->sendMessage("Функция не реализоавана (nodeWillChange)");
  }

  public function mkDir($path, $newDirName) {
    $this->sendMessage("Функция не реализоавана (mkDir)");
  }

  public function getWrapperClassName() {
    $this->sendMessage("Функция не реализоавана (getWrapperClassName)");
  }

  public function initRepository() {

  }

  public function getResourceUrl($path) {
    return $this->urlBase . $path;
  }

  protected function getNodesDiffArray() {
    return array("REMOVE" => array(), "ADD" => array(),
                 "UPDATE" => array());
  }

  public function switchAction($action, $httpVars, $fileVars) {
    $domain = $_SESSION['domain_path'] OR "";
    if ($domain === "")
      $this->sendMessage("Ошибка: Требуется повторная авторизация на сервере");

    $selection = new UserSelection($this->repository);
    $dir = $httpVars["dir"] OR "";

    $selection->initFromHttpVars($httpVars);
    $mess = ConfService::getMessages();


    $pendingSelection = "";
    $logMessage = null;
    $reloadContextNode = false;

    switch ($action) {
      //  Получение списка процессов в очереди
      case "ls":
        $url = '/v1/queue/list';
        $torque_queue =
          fsAccessWrapper_remote::remoteCommand($url, "GET");
        if ($torque_queue->success === "true") {
          $parentAjxpNode = new AJXP_Node('', array());
          AJXP_XMLWriter::renderAjxpHeaderNode($parentAjxpNode);
          foreach ($torque_queue->jobs->items as $item) {
            $newNode = new AJXP_Node($item->id, array());
            $this->loadNodeInfo($newNode, $item);
            AJXP_XMLWriter::renderAjxpNode($newNode);
          }
          AJXP_XMLWriter::close();
        }
        else {
          $this->sendMessage("Ошибка просмотра директории");
          return;
        }
        break;

      //  Получение информации о процессе
      case "get_content":
        $url = "/v1/queue/task/" . $httpVars["file"];
        $response =
          fsAccessWrapper_remote::remoteCommand($url, "GET");
        echo $this->convertTaskInfoToText($response);
        return;

      //  Добавление задачи в очередь
      case "mkdir":
        $script = $httpVars["script"];
        $params = $httpVars["params"];
        $res = $this->create($script, $params);
        if (!isSet($nodesDiffs)) $nodesDiffs =
          $this->getNodesDiffArray();
        if ($res['SUCCESS']) {
          $url = "/v1/queue/task/" . $res["MESSAGE"];
          $response =
            fsAccessWrapper_remote::remoteCommand($url, "GET");
          try {
            $newNode = new AJXP_Node($response->task->id, array());
            $this->loadNodeInfo($newNode, $response->task);
            array_push($nodesDiffs["ADD"], $newNode);
            $logMessage =
              "Задача " . $response->task->id . " успешно добавлена";
          }
          catch (Exception $e) {
            fsAccessWrapper_remote::sendMessage("Ошибка получения информации о созданной задаче");
            return;
          }
        }
        else {
          fsAccessWrapper_remote::sendMessage($res["MESSAGE"]);
          return;
        }
        break;
      //  Удаление задачи из очереди
      case "delete":
        $id = $httpVars["file"];
        if (!isSet($nodesDiffs)) $nodesDiffs =
          $this->getNodesDiffArray();
        $mess = $this->delete($id);
        if (is_null($mess)) {
          array_push($nodesDiffs["REMOVE"], $id);
          $logMessage = "Задача " . $id . " успешно удалена";
        }
        else {
          fsAccessWrapper_remote::sendMessage($mess);
          return;
        }
        break;
    }


    $xmlBuffer = "";
    if (isset($logMessage) || isset($errorMessage)) {
      $xmlBuffer .= AJXP_XMLWriter::sendMessage((isSet($logMessage) ?
        $logMessage : null),
        (isSet($errorMessage) ? $errorMessage : null), false);
    }
    if ($reloadContextNode) {
      if (!isSet($pendingSelection)) $pendingSelection = "";
      $xmlBuffer .= AJXP_XMLWriter::reloadDataNode("",
        $pendingSelection, false);
    }
    if (isSet($reloadDataNode)) {
      $xmlBuffer .= AJXP_XMLWriter::reloadDataNode($reloadDataNode,
        "", false);
    }
    if (isSet($nodesDiffs)) {
      $xmlBuffer .= AJXP_XMLWriter::writeNodesDiff($nodesDiffs,
        false);
    }

    return $xmlBuffer;
  }


  public function convertTaskInfoToText($request) {
    $r = $request->task;
    $res = "";
    $res .= "Имя процесса:\t%s \nСервер:\t%s \nID процесса:\t%s \nВладелец процесса:\t%s \nВремя старта:\t%s \nВремя работы:\t%s";
    $res .= "\nСостояние:\t%s \nФайл с выходными данными:\t%s \nФайл ошибок:\t%s \nЗапускаемый скрипт:\t%s \nИспользуемая память:\t%s \nКол-во узлов:\t%s";
    return sprintf($res, $r->name, $r->server, $r->id, $r->owner,
      $r->tstart, $r->runtime, $r->state,
      $r->outputfile, $r->errorfile, $r->script, $r->mem, $r->nodes);
  }

  //Получение подробной информации
  public function loadNodeInfo(&$ajxpNode, $array) {
    $metaData = array();
    $metaData["is_file"] = true;
    $metaData["text"] = $array->name;
    $metaData["mimestring_id"] = 23;
    $metaData["icon"] = "mime_empty.png";
    $metaData["file_group"] = "unknown";
    $metaData["file_owner"] = "unknown";
    $metaData["ajxp_readonly"] = "true";
    $metaData["file_perms"] = "0777";
    $metaData["ajxp_modiftime"] =
      DateTime::createFromFormat('Y.m.d H:i:s', $array->tstart)
        ->getTimestamp();
    $metaData["ajxp_description"] =
    $metaData["ajxp_relativetime"] = "";
    $metaData["bytesize"] = 10;
    $metaData["filesize"] =
      AJXP_Utils::roundSize($metaData["bytesize"]);

    $metaData["jid"] = $array->jid;
    $metaData["id"] = $array->id;
    $metaData["owner"] = $array->owner;
    $metaData["state"] = $array->state;

    $ajxpNode->mergeMetadata($metaData);
  }


  public function delete($id) {
    $url = "/v1/queue/task/" . $id;
    $request =
      fsAccessWrapper_remote::remoteCommand($url, "DELETE", array());
    if ($request->success !== 'true')
      return $request->message;
    return null;
  }

  public function create($script, $params) {
    $url = "/v1/queue/task";
    $params = str_replace(" ", "", $params);
    $params_splited = array();
    $params = preg_split('/[;]/', $params);
    foreach ($params as $param) {
      if ($param === "") continue;
      $param_arr = preg_split('/[=]/', $param);
      if (count($param_arr) != 2)
        return array('SUCCESS' => false,
                     "MESSAGE" => "Не верно задан параметр");
      $params_splited[$param_arr[0]] = $param_arr[1];
    }
    $request = fsAccessWrapper_remote::remoteCommand($url, "POST",
      array("script_path" => $script,
            "arguments" => $params_splited));
    if ($request->success === "true") {
      $id = explode(".", $request->message);
      return array('SUCCESS' => true, "MESSAGE" => $id[0]);
    }
    else
      return array('SUCCESS' => false,
                   "MESSAGE" => $request->message);

  }

  public function date_modif($file) {
    $tmp = 0;
    return $tmp;// date("d,m L Y H:i:s",$tmp);
  }

  public function isWriteable($dir, $type = "dir") {
    return false;
  }

  public function isText($type) {
    $types = array(23, 10, 11, 14, 15, 21, 22, 58, 64);
    return in_array($type, $types);
  }

}
